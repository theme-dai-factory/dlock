# 锁屏自适应布局框架DLock

## 背景介绍

<p style="width:400px;">
<div style="float:left;clear:both;" align="center" >
<img width="200" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%A4%B4%E5%83%8F.jpg" hspace="8"><br/>
<b>戴瑞豪，兴趣创业者，技术宅
</b><br/>
</div>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我是Darry，主题戴工厂的厂长，大家也可以叫我戴厂长（戏称），我们从2018年开始为国内的几大安卓（及鸿蒙，下同）手机厂商制作官方手机主题，有着丰富的跨平台经验。「DLock」这个开源框架经过我们团队多年打磨，是一个为安卓锁屏引擎所写的自适应组件库，面向「锁屏开发者」以及「主题设计师」，提供语义化的布局参数，让元素在多机型的定位和适配变得简单，同时还支持在单一手机上模拟全分辨率、跨平台的仿真效果，大大提高了适配效率，降低了真机测试成本。
</p>

## 开源赞助伙伴
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DLock是一个遵循MIT协议的开源项目，完全免费，开发并维护这么一个项目并持续引入更多便捷的新功能需要耗费巨大的精力，只有在我们的赞助伙伴慷慨的资金支持下才成为可能（即便是请我喝一杯咖啡的金额也是对我精神上莫大的支持与鼓励）。

- 为了表示感谢，我们将在本页面展示并持续更新赞助伙伴（可选择是否展示头像或品牌logo），在开源中国 Gitee这一全国知名的开源平台上的赞助，可以提高您的品牌知名度。

- 如果您使用DLock便捷地构建锁屏，并由此带来创收，或者是开发效率上的提升，那么赞助DLock更具现实意义：这是对我本人劳动成果的认可和点赞，也确确实实的为您的生产带来了帮助，同时，您的支持将继续鼓励我创作更多的新功能。

- 如果您是一名主题设计师，通过赞助曝光可以获得高质量的流量，因为我们这里的访问者大多数都是主题爱好者。
- 还能加入赞助伙伴社群，进行交流讨论、建言献策。

<div align="center">
<img height="266" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%8F%8C%E7%A0%81%E6%94%B6%E6%AC%BE%E7%A0%81.jpg"><br/>
<b>赞助渠道：微信&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;赞助渠道：支付宝
</b>
</div>

<div align="center">
<b>❤️ 请赞助后的朋友加我微信rickydream2020 ❤️</b>
</div>

### Darry和他的赞助伙伴们

| 榜单排行 | 赞助伙伴 | 附图或附言（可跳转至指定页面） |
|---|---|---|
|     1      | ●)o(● | |
|     2      | Darry |<a href="https://gitee.com/theme-dai-factory/dlock/blob/master/README.md" target="_blank"><img width="100" src="https://i.328888.xyz/2023/02/01/Iy1mP.jpeg"></a>我本人啦，有梦就去追，与君共勉！~|



## 文档
觉得文档枯燥？可以直接看我的视频讲解，还有更多主题设计知识：
- <a href="https://www.bilibili.com/video/BV1Zx4y1L7h3/" target="_blank">《锁屏自适应布局框架DLock》</a>
- <a href="https://www.bilibili.com/video/BV1F8411h7dq/" target="_blank">《全生态手机主题通用设计稿》</a>
- <a href="https://www.bilibili.com/video/BV1p24y1D7BK/" target="_blank">《生动讲解点九图究竟是怎么回事》</a>
- <a href="https://www.bilibili.com/video/BV1834y1X7Fn/" target="_blank">《用AE实现二次元视频静转动の头发丝动效》</a>


### 几点说明

1.   **DLock框架**  由  **manifest.xml（初始化锁屏脚本文件）**  和  **bg_tool（组件资源文件夹）**  构成。
2.  **manifest.xml（初始化锁屏脚本文件）** 已经集成了组件代码库，您只需在变量声明区、控件摆放区这两个位置书写代码，并绑定对应的布局参数即可。
<img src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E6%95%B4%E4%BD%93%E9%A2%84%E8%A7%88.jpg" hspace="8">

3.  DLock框架采用低耦合、低侵入性的架构逻辑，只需要绑定参数变量即可自适应布局，因此不仅支持新代码的构建，还可以较方便的移植到老代码中（后面有专门的小节讲到）。
4.  因为MIT协议，您的代码第2行需要保留这句注释：

```
<!-- 本锁屏响应式布局DLock框架由【主题戴工厂】开源，如使用需保留此句话，合作咨询请关注公众号【主题戴工厂】 -->
```
5.  开启或者关闭测试框架，只需要将moni_status改为1或0：

```
<!-- 模拟框架开启状态 -->
<Var expression="1" name="moni_status"/>

<!-- 模拟框架关闭状态 -->
<Var expression="0" name="moni_status"/>
```

### 响应式布局

响应式布局只解决一个问题，那就是：对于每一个 **元素** 或者是每一个 **组件** （由多个元素组成的元素集合，以下统称组件，后面会解释）来说，如果我们想把它放到锁屏上，它相对于锁屏的高度位置，是 _顶部对齐_ 的、 _居中对齐_ 的，还是 _底部对齐_ 的？ 

#### 锁屏坐标系

常规的锁屏坐标系的原点在屏幕左上角，越往下，y的坐标越大，越往右，x的坐标越大。

<img width="300" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%9D%90%E6%A0%87%E7%B3%BB.png" hspace="8">

#### 「组件」相对于「锁屏坐标系」的位置
在我们开始写代码的时候，y坐标是直接写数值n，这其实就是相对于坐标系顶部的距离为n，代码及效果如下：

```
<!-- 放在距离屏幕顶部往下200像素的位置 -->
<Image x="200" y="200" src="blue.png">
```
<img width="400" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E9%A1%B6%E9%83%A8.jpg" hspace="8">

如果想写距离底部，就会用到#sh去减，代码及效果如下：

```
<!-- 放在距离屏幕底部往上200像素的位置 -->
<Image x="200" y="#sh-200" src="blue.png">
```
<img width="400" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%BA%95%E9%83%A8.jpg" hspace="8">

如果想写距离正中位置，就会用到#sh/2去加或者减，代码及效果如下：

```
<!-- 放在距离屏幕正中往下200像素的位置 -->
<Image x="200" y="#sh/2+200" src="blue.png">
```
<img width="400" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%B1%85%E4%B8%AD.jpg" hspace="8">

#### 语义化变量
现在我们用语义化变量替换: 
| 参数名       | 含义     |
|-----------|--------|
|  **#top_base**  |  **顶部对齐基准**  |
|  **#bot_base**  |  **底部对齐基准**  |
|  **#mid_base**  |  **居中对齐基准**  |


```
<!-- 放在距离屏幕顶部往下200像素的位置 -->
<Image x="200" y="#top_base+200" src="blue.png">

<!-- 放在距离屏幕底部往上200像素的位置 -->
<Image x="200" y="#bot_base-200" src="blue.png">

<!-- 放在距离屏幕正中往下200像素的位置 -->
<Image x="200" y="#mid_base+200" src="blue.png">
```
<img src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E6%96%B0%E5%9F%BA%E5%87%86.jpg" hspace="8">

 **使用语义化变量有几点好处：** 
1. 不需要关注内部公式是如何计算的，只需要关注你的组件与屏幕的相对位置关系。
2. 语义化变量内部其实经过了一系列判断，无论是在真机实际屏幕大小，还是在模拟各分辨率大小的情况下，都能使绑定的组件处在正确定义的相对位置中。

#### 老代码移植
在了解了上述基础用法后，我们可以将需要模拟测试的老代码，整体复制粘贴到manifest.xml的中间，再使用现代化的代码编辑器（推荐VS Code或者Sublime Text），将组件折叠至只有一行，排除干扰，也是为了利于阅读。（请注意，为了能够被正确的收起，缩进关系一定要正确，如果不想手动缩进，可以使用xml格式化工具，自动完成正确缩进格式）
<img src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E8%80%81%E4%BB%A3%E7%A0%81.jpg" hspace="8">

接下来，我们就可以对每一个组件进行语义化绑定，即这个组件如果y坐标原先是纯数字，那就改为#top_base+纯数字，如果原先为#sh-，那就改为#bot_base-，如果原先为#sh/2-，那就改为#mid_base-，改造完成后的代码如下：
<img src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E6%94%B9%E9%80%A0%E5%90%8E.jpg" hspace="8">

请注意，在这里我们只改造最外层的「组件」，组件内部的各元素，它们的y坐标并不是相对于屏幕的位置，而是相对于该「组件」容器的位置，所以会与组件一起变动位置，无需关注；当然，这需要平时有良好的代码书写习惯，如果本身拆分的元素直接写在最外层，那所有在最外层的「元素」都要绑定语义化变量。


### 模拟分辨率

#### 真机看效果
在完成语义化变量的绑定后，无论你是哪家厂商的手机，正确的放入manifest.xml和bg_tool文件夹后，都可以在真机上应用，应用当前主题后，双击屏幕任意位置，将出现菜单选项，你就可以开始测试了，你可以选择三家厂商的不同分辨率，我们还提供了【恢复全屏】按钮，还提供了入口可以输出测试数据，默认会显示当前屏幕的宽高，以及模拟宽高：
<p style="width:400px;">
<div style="float:left;clear:both;" align="center" >
<img width="400" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%88%86%E8%BE%A8%E7%8E%871.jpg" hspace="8"><br/>
<b>该主题名为《国潮风 钱兔似锦》
</b><br/>
</div>

大屏幕手机展示小分辨率时，会居中显示，上下留黑：
<p style="width:400px;">
<div style="float:left;clear:both;" align="center" >
<img width="200" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%88%86%E8%BE%A8%E7%8E%872.jpg" hspace="8"><br/>
<b>此套主题刚好各组件都是居中对齐，并不明显，实际上各元素会跟随它们所绑定的屏幕顶部、居中、底部基准变化而变化
</b><br/>
</div>

小屏幕手机展示大分辨率时，会分两屏展示，首屏顶对齐，底部被裁切，次屏显示底部的一半，也是顶对齐展示，下方留黑。此时菜单选项会有一个上下按钮，方便切换：
<p>
<div style="float:left;clear:both;" align="center" >
<img src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/%E5%88%86%E8%BE%A8%E7%8E%873.jpg" hspace="8"><br/>
<b>「次屏」实际上居中对齐的基准上移了，所以整体上移</b><br/>
</div>

#### 安全区对照

针对指定厂商的指定分辨率（绿厂2种、蓝厂1种），我们也加入了官方提供的模板，并进行了对齐模式的换算，保证所见即所得，正确表示当前分辨率的安全区位置：
<img src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/fbl.jpg" hspace="8">

#### oppo多分辨率文件夹计算器

众所周知，oppo支持多分辨率xml拆分放置，但是以一个约分后的数值命名每一个分辨率的文件夹，我们已经根据宽高比例，换算出了当前分辨率所对应的文件夹名称：

<img width="400" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/oppo1.jpg" hspace="8">

与此同时，oppo分辨率宽为1440的尺寸，其实在锁屏代码第3行，我们定义了screen width = 1080，所以在底层代码逻辑里，所谓的1440x3168，不过是1080x2376的等比撑大显示而已，所以在DLock模拟框架中，我们针对1440宽的几个分辨率，也都是以1080宽等比换算显示，在数据输出控制台中，你可以看到它们的实际宽高：

<img width="400" src="https://gitee.com/theme-dai-factory/dlock/raw/master/%E6%96%87%E6%A1%A3%E6%89%80%E7%94%A8%E5%9B%BE%E7%89%87/oppo2.jpg" hspace="8">


### 规定时间

绑定框架中的时间、电量参数，在真机测试中点击【规定时间】，就可以一键修改时间为厂商要求的时间，同时电量改为100%，方便生成各厂家的锁屏预览图：

#### 厂商规定
| 厂商   | 规定时间                |
|------|---------------------|
| 华为   | 08:08               |
| vivo | 10:00 2020年3月21日星期六 |
| oppo | 无限制，当前和vivo取齐       |

#### 相关参数
| 参数名    | 含义             |
|--------|----------------|
| #shiz  | 时钟，已关联24/12小时制 |
| #fenz  | 分钟             |
| #miaoz | 秒钟             |
| #dl    | 电量             |


由于系统限制，分钟时间只能持续一分钟，如果系统时间发生改变，修改的参数也会改变，此时需要再次点击【固定时间】按钮。
请注意，该功能目前处于优化迭代中，后续将加入新的自适应布局配置，以规避顶部状态栏。


### 更多功能，敬请期待
todo：图形化锁屏开发工具


## License

[MIT](https://opensource.org/licenses/MIT)

Copyright (c) 2018-present, Darry Dai